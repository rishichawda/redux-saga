import _ from "lodash";
import uuid from "uuid";
import {
  PUSH_TO_QUEUE,
  WEBSOCKET_MESSAGE_SENT,
  WEBSOCKET_MESSAGE,
  WEBSOCKET_MESSAGES_READ,
  WEBSOCKET_UNSET_CURRENT_CHAT
} from "../shared/actionTypes";

const initialState = {
  messages: {},
  queue: {},
  unreadCount: {},
  currentUserId: "",
  unreadMessagesCount: 0,
  currentViewingChatId: null
};

const addMetaToMessage = message => ({
  ...message,
  _id: message._id || uuid.v1(),
  user: {
    _id: message.author
  },
  created: message.created || new Date().toISOString()
});

const removeMessageFromQueue = (state, message) => {
  const previousQueuedMessages = state.queue[message.chat_id] || [];
  if (previousQueuedMessages) {
    return {
      ...state.queue,
      [message.chat_id]: _.filter(
        previousQueuedMessages,
        previousQueuedMessage => previousQueuedMessage._id !== message._id
      )
    };
  }
  return state.queue;
};

const addNewMessageToChat = (state, message, isUnread) => {
  const newMessage = addMetaToMessage({
    ...message,
    sent: true
  });
  const previousSentMessages = state.messages[newMessage.chat_id] || [];
  const previousUnreadCount = state.unreadCount[newMessage.chat_id] || 0;
  return {
    ...state,
    unreadCount: {
      ...state.unreadCount,
      [newMessage.chat_id]: isUnread
        ? previousUnreadCount + 1
        : previousUnreadCount
    },
    messages: {
      ...state.messages,
      [newMessage.chat_id]: _.concat(newMessage, previousSentMessages)
    },
    queue: removeMessageFromQueue(state, newMessage),
    unreadMessagesCount: isUnread
      ? state.unreadMessagesCount + 1
      : state.unreadMessagesCount
  };
};

const addNewMessageToQueue = (state, { payload }) => {
  const newQueuedMessage = {
    ...payload.message,
    sent: false
  };
  const previousQueuedMessages = state.queue[newQueuedMessage.chat_id] || [];
  return {
    ...state,
    queue: {
      ...state.queue,
      [newQueuedMessage.chat_id]: _.concat(
        addMetaToMessage(newQueuedMessage),
        previousQueuedMessages
      )
    }
  };
};

const updateUnreadMessages = (state, chatId) => {
  const previousUnreadCountForChatId = state.unreadCount[chatId] || 0;
  const updatedUnreadMessagesCount = Math.max(
    0,
    state.unreadMessagesCount - previousUnreadCountForChatId
  );
  return {
    ...state,
    unreadCount: {
      ...state.unreadCount,
      [chatId]: 0
    },
    currentViewingChatId: chatId,
    unreadMessagesCount: updatedUnreadMessagesCount
  };
};

const unsetCurrentViewingChatId = state => {
  return {
    ...state,
    currentViewingChatId: null
  };
};

export default function(state = initialState, action) {
  switch (action.type) {
    case WEBSOCKET_MESSAGE_SENT:
      return addNewMessageToChat(state, action.payload.message, false);

    case PUSH_TO_QUEUE:
      return addNewMessageToQueue(state, action);

    case WEBSOCKET_MESSAGE:
      const newMessage = JSON.parse(action.payload.data).message;
      if (newMessage.author === state.currentUserId) {
        return state;
      }
      return addNewMessageToChat(
        state,
        newMessage,
        newMessage.chat_id !== state.currentViewingChatId
      );

    case WEBSOCKET_MESSAGES_READ:
      return updateUnreadMessages(state, action.payload.chatId);

    case WEBSOCKET_UNSET_CURRENT_CHAT:
      return unsetCurrentViewingChatId(state);

    default:
      return state;
  }
}
