import { createStore, applyMiddleware, combineReducers, compose } from "redux";
import reducer from "./reducer";
import ReduxLogger from "redux-logger";
import rootSaga from "../sagas/saga";
import createSagaMiddleware from "redux-saga";
import { createOffline } from "@redux-offline/redux-offline";
import offlineConfig from "@redux-offline/redux-offline/lib/defaults";

const rootReducer = combineReducers({
  webSocket: reducer
});
const sagaMiddleware = createSagaMiddleware();
const { middleware, enhanceReducer, enhanceStore } = createOffline(
  offlineConfig
);

const store = createStore(
  enhanceReducer(rootReducer),
  compose(
    enhanceStore,
    applyMiddleware(sagaMiddleware, ReduxLogger, middleware)
  )
);

sagaMiddleware.run(rootSaga);

export default store;
