import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import {
  WEBSOCKET_DISCONNECT,
  WEBSOCKET_OPEN,
  WEBSOCKET_SEND,
  WEBSOCKET_CONNECT
} from "../shared/actionTypes";
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: "",
      chat_id: 1
    };
  }
  sendMessage = e => {
    e.preventDefault();
    this.props.send({
      message: this.state.message,
      chat_id: this.state.chat_id
    });
  };
  render() {
    return (
      <div>
        <button onClick={() => this.props.connect()}>connect</button>
        <button onClick={() => this.props.disconnect()}>disconnect</button>
        <button onClick={() => this.props.open()}>open</button>
        <form onSubmit={this.sendMessage}>
          <input
            type="text"
            name="message"
            placeholder="message"
            value={this.state.message}
            onChange={({ target }) => {
              this.setState({ message: target.value });
            }}
            required
          />
          <input
            type="number"
            name="chatid"
            placeholder="chat_id"
            value={this.state.chat_id}
            onChange={({ target }) => {
              this.setState({ chat_id: target.value });
            }}
            required
          />
          <button type="submit">send</button>
        </form>
      </div>
    );
  }
}

const getActions = dispatch => ({
  connect: bindActionCreators(
    () => ({
      type: WEBSOCKET_CONNECT,
      payload: { url: "ws://localhost:8002" }
    }),
    dispatch
  ),
  disconnect: bindActionCreators(
    () => ({ type: WEBSOCKET_DISCONNECT }),
    dispatch
  ),
  open: bindActionCreators(() => ({ type: WEBSOCKET_OPEN }), dispatch),
  send: bindActionCreators(
    ({ message, chat_id }) => ({
      type: WEBSOCKET_SEND,
      payload: {
        message: { message, chat_id, author: "random", created: Date.now() }
      }
    }),
    dispatch
  )
});

export default connect(
  null,
  getActions
)(App);
