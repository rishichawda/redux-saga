import {
  select,
  all,
  put,
  take,
  takeEvery,
  call,
  spawn,
  fork,
  cancel,
  cancelled
} from "redux-saga/effects";
import _ from "lodash";
import {
  WEBSOCKET_CONNECT,
  WEBSOCKET_DISCONNECT,
  WEBSOCKET_SEND,
  WEBSOCKET_OPEN,
  WEBSOCKET_CLOSED,
  WEBSOCKET_CONNECTING,
  WEBSOCKET_MESSAGE,
  WEBSOCKET_MESSAGE_SENT,
  PUSH_TO_QUEUE,
  MESSAGE_HANDLE,
  WEBSOCKET_CONNECTION_ERROR,
  NETWORK_STATUS_CHANGED
} from "../shared/actionTypes";
import { eventChannel, delay } from "redux-saga";

import * as WebSocketConnectionStates from "../shared/wsConnectionStates";

// Globals
const RETRY_SATURATION_TIME = 120000;
const RETRY_BACKOFF_TIME_FACTOR = 2;

// Functions for getting values from store state
const getPendingActionQueue = state => state.webSocket.queue;

const getConnected = state => state.offline.online;

// generators to dispatch actions
function* connectWebsocket(config) {
  yield put({
    type: WEBSOCKET_CONNECT,
    payload: config
  });
}

function* sendMessages(message) {
  console.log(message);
  yield put({
    type: WEBSOCKET_SEND,
    payload: { message }
  });
}

// subscription functions for websocket events
function websocketOpenSubscription(socket) {
  return eventChannel(emitter => {
    socket.onopen = event => {
      emitter({
        type: WEBSOCKET_OPEN,
        payload: {
          timestamp: Date.now(),
          event
        }
      });
    };
    return () => {
      console.log("unsubscribe to ws:open event");
    };
  });
}

function websocketCloseSubscription(socket) {
  return eventChannel(emitter => {
    socket.onclose = event => {
      emitter({
        type: WEBSOCKET_CLOSED,
        payload: {
          timestamp: Date.now(),
          event
        }
      });
    };
    return () => {
      console.log("unsubscribe to ws:close event");
    };
  });
}

function websocketMessageSubscription(socket) {
  return eventChannel(emitter => {
    socket.onmessage = event => {
      emitter({
        type: MESSAGE_HANDLE,
        payload: event
      });
    };
    return () => {
      console.log("unsubscribe to ws:message event");
    };
  });
}

// Helper functions
export function* clearActionQueue() {
  const pendingChats = yield select(getPendingActionQueue);
  yield all(
    _.map(
      [].concat.apply(
        [],
        Object.keys(pendingChats).map(key => {
          return pendingChats[key];
        })
      ),
      message => {
        return call(sendMessages, message);
      }
    )
  );
}

function* websocketEventSaga(event) {
  while (true) {
    const { type, payload } = yield take(event);
    yield put({ type, payload });
  }
}

const extractArgs = config => {
  if (config.args) {
    return config.args;
  }
  if (config.url) {
    return [config.url];
  }
  return [];
};

export const createWebsocket = payload => {
  const args = extractArgs(payload);
  const websocket = payload.websocket ? payload.websocket : WebSocket;
  let socket = new websocket(...args);
  return {
    socket,
    config: { ...payload, shouldRetry: true, retryTimeOff: 1000 }
  };
};

function* initialize(config) {
  let { socket } = createWebsocket(config);

  const [opened, closed, message] = yield all([
    call(websocketOpenSubscription, socket),
    call(websocketCloseSubscription, socket),
    call(websocketMessageSubscription, socket)
  ]);

  yield spawn(websocketEventSaga, opened);
  yield spawn(websocketEventSaga, closed);
  yield spawn(websocketEventSaga, message);

  return { socket, config };
}

// Functions to handle connection and messages
function* websocketConnect(action) {
  let websocket = null;
  yield put({
    type: WEBSOCKET_CONNECTING,
    payload: {
      timestamp: Date.now(),
      websocketConfig: action.payload
    }
  });
  const isConnected = yield select(getConnected);
  if (
    isConnected &&
    (!websocket || websocket.readyState === WebSocketConnectionStates.CLOSED)
  ) {
    try {
      websocket = yield call(initialize, action.payload);
    } catch (err) {
      console.log("error occured while initializing socket", err);
    } finally {
      if (
        websocket.socket.readyState !== WebSocketConnectionStates.OPEN &&
        websocket.socket.readyState !== WebSocketConnectionStates.CONNECTING
      ) {
        yield call(websocketConnect, action);
      }
    }
  } else {
    yield put({
      type: WEBSOCKET_CONNECTION_ERROR,
      payload: {
        timestamp: Date.now()
      }
    });
  }
  return websocket;
}

function* websocketsendmessage({ socket, config }, { payload }) {
  const isConnected = yield select(getConnected);
  if (isConnected && socket.readyState === WebSocketConnectionStates.OPEN) {
    console.warn("message is being sent");
    socket.send(JSON.stringify(payload));
    yield put({
      type: WEBSOCKET_MESSAGE_SENT,
      payload
    });
  } else {
    console.warn("message is being pushed");
    try {
      yield call(connectWebsocket, config);
    } catch (err) {
      console.log("connecting to websocket failed", err);
    }
    yield put({
      type: PUSH_TO_QUEUE,
      payload
    });
  }
}

function* websocketSend(websocket) {
  try {
    yield takeEvery(WEBSOCKET_SEND, websocketsendmessage, websocket);
  } catch (err) {
    console.log("error in w:send saga", err);
  } finally {
    if (yield cancelled()) {
      console.warn("ws:send saga closed");
    }
  }
}

// Saga to handle websocket close event
export function* websocketClosed({ config }) {
  yield take(WEBSOCKET_CLOSED);
  let { shouldRetry, retryTimeOff } = config;
  if (shouldRetry) {
    retryTimeOff = Math.min(
      RETRY_BACKOFF_TIME_FACTOR * retryTimeOff,
      RETRY_SATURATION_TIME
    );
    yield call(delay, retryTimeOff);
    yield call(connectWebsocket, config);
  }
}

// Saga to handle incoming messages
function* messageHandlerSaga() {
  try {
    while (true) {
      const { payload } = yield take(MESSAGE_HANDLE);
      yield put({
        type: WEBSOCKET_MESSAGE,
        payload: {
          timestamp: new Date(),
          data: payload.data,
          event: payload
        }
      });
    }
  } catch (err) {
    console.log("error in ws:message_handler saga", err);
  } finally {
    if (yield cancelled()) {
      console.warn("ws:message_handler saga closed");
    }
  }
}

// Saga for watching network status changes
function* watchNetworkStatusSaga() {
  try {
    while (true) {
      let { payload } = yield take(NETWORK_STATUS_CHANGED);
      if (payload.online) {
        try {
          yield call(clearActionQueue);
        } catch (err) {
          console.err("error occured in clear:queue on net:status change");
        }
      }
    }
  } catch (err) {
    console.err("error occured in net:status saga", err);
  }
}

// Saga for handling websocket connection
export function* websocketConnectionSaga() {
  let websocket;
  while (true) {
    const connectAction = yield take(WEBSOCKET_CONNECT);
    try {
      websocket = yield call(websocketConnect, connectAction);
    } catch (err) {
      console.log("websocket connect saga failed", err);
    }

    const net_stat = yield fork(watchNetworkStatusSaga);
    yield fork(websocketClosed, websocket);
    const message_handler = yield fork(messageHandlerSaga);
    const message_send = yield fork(websocketSend, websocket);

    yield take(WEBSOCKET_OPEN);
    websocket.config.shouldRetry = true;
    websocket.config.retryTimeOff = 1000;
    yield call(clearActionQueue);

    yield take(WEBSOCKET_DISCONNECT);
    yield cancel(message_handler);
    yield cancel(message_send);
    yield cancel(net_stat);
    try {
      websocket.config.shouldRetry = false;
      if (websocket.socket) {
        websocket.socket.close();
        websocket.socket = null;
      }
    } catch (err) {
      console.log("websocket disconnect saga failed", err);
    }
  }
}

export default function* rootSaga() {
  yield fork(websocketConnectionSaga);
}
